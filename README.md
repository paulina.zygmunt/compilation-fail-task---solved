Zadanie:

Poprawić 
plik test_kompilacji_fail.c

w ten sposób, aby output kompilacji był czysty.

1) nie można zakomentowywać kodu,
2) można zmieniać lub dopisywać linie kodu

Nalezy:

- pobrac caly folder, lacznie z plikiem makefile
- skompilowac plik test_kompilacji_fail-solved.c wpisujac w linii komend: 
make
- uruchomic program bin_kompilation_success z parametrem w postaci liczby, np.: 
./bin_kompilation_success 43