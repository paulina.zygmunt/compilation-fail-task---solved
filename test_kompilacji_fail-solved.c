#include <stdio.h>

int my_function(int *x) {
    return *x=*x+1;
}

int main(int argc, char ** argv)
{
    int x,y;

	/* function below gets 
	function call argument as 'y' 
	and converts this string to an int 
	for later calculations */
	sscanf(argv[1], "%d", &y);

    x=y;
    my_function(&x);

	printf("modified: %d, original: %d\n", x, y);

    return 0;
}
